status = []

from buildbot.status import html
from buildbot.status.web.authz import Authz
from buildbot.status.web.auth import BasicAuth
from buildbot.status.mail import MailNotifier
from buildbot.status.client import PBListener

from fenicsbbotcfg import builders

users = [('dev', 'bbot!')]  # it's not *that* secret..
authz = Authz(
    auth=BasicAuth(users),
    forceBuild=True,
    forceAllBuilds=True,
    cancelPendingBuild=True,
    stopChange=True,
    stopBuild=True)

status.append(html.WebStatus(
    http_port=8010,
    authz=authz,
    order_console_by_time=True,
    projects={"dijitso": "https://bitbucket.org/fenics-project/dojitso",
              "dolfin": "https://bitbucket.org/fenics-project/dolfin",
              "ffc": "https://bitbucket.org/fenics-project/ffc",
              "fiat": "https://bitbucket.org/fenics-project/fiat",
              "instant": "https://bitbucket.org/fenics-project/instant",
              "mshr": "https://bitbucket.org/benjamik/mshr",
              "ufl": "https://bitbucket.org/fenics-project/ufl",
              },
    ))

status.append(PBListener(port=9988))
