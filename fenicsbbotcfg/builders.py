import textwrap
import itertools
import os

from buildbot.process import factory
from buildbot.process.factory import BuildFactory
from buildbot.plugins import steps
from buildbot.steps.shell import Configure, Compile, Test, ShellCommand
from buildbot.steps.transfer import FileDownload, FileUpload, DirectoryUpload
from buildbot.steps.python_twisted import Trial
from buildbot.steps.python import PyFlakes
from buildbot.process.factory import Distutils
from buildbot.steps.slave import RemoveDirectory, MakeDirectory
from buildbot.steps.master import MasterShellCommand

from fenicsbbotcfg.slaves import slaves, get_slaves, names

# slaves seem to have a hard time fetching from Bitbucket, so retry
# every 5 seconds, 5 times
GIT_RETRY = (5,5)

# This is the folder on fenicsproject.org where public downloads
# should be uploaded (like snapshots, documentation, etc.).
public_html = "~/buildmaster/public_html"

def update_env(prefix, env, os="linux", pyver="2.7"):
    tmp = env.copy()
    tmp["PATH"] = [prefix + "/bin", "${PATH}"] + tmp.get("PATH", [])
    if os == "osx":
        tmp["DYLD_LIBRARY_PATH"] = [prefix + "/lib",
                                    "${DYLD_LIBRARY_PATH}"] + \
                                     tmp.get("LD_LIBRARY_PATH", [])
    else:
        tmp["LD_LIBRARY_PATH"] = [prefix + "/lib",
                                  "${LD_LIBRARY_PATH}"] + \
                                  tmp.get("LD_LIBRARY_PATH", [])
    tmp["PKG_CONFIG_PATH"] = [prefix + "/lib/pkgconfig",
                              "${PKG_CONFIG_PATH}"] + \
                              tmp.get("PKG_CONFIG_PATH", [])
    tmp["PYTHONPATH"] = [prefix + "/lib/python" + pyver + "/site-packages",
                         "${PYTHONPATH}"] + tmp.get("PYTHONPATH", [])
    return tmp

class PythonBuild(ShellCommand):
    description = ["building"]
    descriptionDone = ["build"]
    name = "build"

class PythonInstall(ShellCommand):
    description = ["installing"]
    descriptionDone = ["install"]
    name = "install"

class MakeInstall(ShellCommand):
    haltOnFailure = 1
    description = ["installing"]
    descriptionDone = ["install"]
    name = "install"

def addpath(envvar, path, env=None):
    """Adds a path to an environment variable."""
    if env is None:
        env = os.environ
    envval = env.get(envvar, path)
    parts = envval.split(os.pathsep)
    parts.insert(0, path)
    # remove duplicate entries:
    i = 1
    while i < len(parts):
        if parts[i] in parts[:i]:
            del parts[i]
        elif envvar == 'PYTHONPATH' and parts[i] == "":
            del parts[i]
        else:
            i += 1
    envval = os.pathsep.join(parts)
    env[envvar] = envval

builders = []

######## BuildFactory Factories

def mk_python_factory(project, branch="master", install_prefix=None,
                      test=True, env=None, **kwargs):
    python = kwargs.get("python", os.environ.get("PYTHON", "python"))

    f = factory.BuildFactory()

    # Checkout
    f.addStep(steps.Git(repourl="https://bitbucket.org/fenics-project/%s.git" % project,
                        branch=branch, mode='full', retry=GIT_RETRY))

    if project == "instant":
        # Clean instant cache
        f.addStep(ShellCommand(command=["instant-clean"],
                               description="instant-clean",
                               descriptionDone="instant-clean",
                               name="instant-clean",
                               flunkOnFailure=False,
                               haltOnFailure=False,
                               warnOnFailure=True))

    # Build (not really needed)
    f.addStep(PythonBuild(command=[python, "setup.py", "build"]))

    # Install
    f.addStep(PythonInstall(command=[python, "setup.py", "install",
                                     "--prefix=" + install_prefix]))

    if test:
        logfiles = {}
        workdir = "build/test"
        test_cmd = ["python", "test.py"]
        if project == "dijitso":
            test_cmd = ["./runtests.sh"]
            logfiles={"output.0": "output.0",
                      "output.1": "output.1",
                      "output.2": "output.2",
                      "output.3": "output.3",
                      "output.4": "output.4",
                      "output.5": "output.5",
                      "output.6": "output.6",
                      "output.7": "output.7",
                      "output.8": "output.8",
                      "output.9": "output.9",
                      "output.10": "output.10",
                      "output.11": "output.11",
                      "output.12": "output.12",
                      "output.13": "output.13",
                      "output.14": "output.14",
                      "output.15": "output.15",
                      }
        # FIXME: Instant should rename run_tests.py to test.py
        if project == "instant":
            test_cmd = ["python", "run_tests.py"]
        if project == "ffc":
            logfiles = {"fiat_errors.log": "unit/fiat_errors.log",
                        "error.log": "regression/error.log"}
        if project == "ufl":
            test_cmd = ["python", "-m", "pytest"]
        f.addStep(Test(command=test_cmd, workdir=workdir,
                       logfiles=logfiles, lazylogfiles=True,
                       timeout=15*60))

        # Run tests for uflacs in ffc
        if project == "ffc":
            test_cmd = ["python", "-m", "pytest"]
            uflacs_workdir = workdir + "/uflacs"
            logfiles = {"cpp_tests.log": "cpp_tests.log",
                        "python_tests.log": "python_tests.log"}
            f.addStep(Test(command=test_cmd, workdir=uflacs_workdir,
                           logfiles=logfiles, lazylogfiles=True,
                           timeout=15*60))

        # Upload coverage report for dijitso
        if project == "dijitso":
            masterdest = public_html + "/cov/dijitso-"
            f.addStep(DirectoryUpload(slavesrc="htmlcov",
                                      masterdest=masterdest + branch + "/htmlcov",
                                      url="http://fenicsproject.org:8010/cov/dijitso-%s/htmlcov" % branch,
                                      workdir=workdir))

            # Set permission on master
            f.addStep(MasterShellCommand(command="chmod -R a+rX %s/cov" % public_html))


    return f

def mk_mshr_factory(branch="default", install_prefix=None,
                    test=True, **kwargs):
    make = kwargs.get("make", os.environ.get("MAKE", "make"))
    python = kwargs.get("python", os.environ.get("PYTHON", "python"))

    f = factory.BuildFactory()

    # Checkout
    f.addStep(steps.Git(repourl="https://bitbucket.org/fenics-project/mshr.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Make build dir
    builddir = "build/build"
    f.addStep(MakeDirectory(dir=builddir, hideStepIf=True))

    # Configure
    cmake_args = ["-DCMAKE_INSTALL_PREFIX:PATH=" + install_prefix,
                  "-DENABLE_TESTS:BOOL=TRUE",
                  "-DCMAKE_BUILD_TYPE:STRING=Debug"]
    f.addStep(Configure(command=["cmake"] + cmake_args + [".."],
                        workdir=builddir,
                        logfiles={"CMakeCache.txt": "CMakeCache.txt",
                                  "CMakeError.log": "CMakeFiles/CMakeError.log"},
                        lazylogfiles=True))

    # Compile
    f.addStep(Compile(command=[make], workdir=builddir, timeout=50*60))

    # Install
    f.addStep(MakeInstall(command=[make, "install"], workdir=builddir))

    # Test
    if test:
        f.addStep(ShellCommand(command=[make, "test"],
                               description="make test",
                               descriptionDone="make test",
                               name="make test",
                               workdir=builddir,
                               logfiles={"LastTest.log": "Testing/Temporary/LastTest.log"},
                               lazylogfiles=True,
                               haltOnFailure=True, flunkOnFailure=True,
                               timeout=50*60))

    return f                                                                         

def mk_dolfin_quick_factory(branch="master", install_prefix=None,
                            test=True, **kwargs):
    make = kwargs.get("make", os.environ.get("MAKE", "make"))
    extra_make_args = kwargs.get("dolfin_make_args", None)
    extra_cmake_args = kwargs.get("dolfin_cmake_args", None)
    cmake_build_type = kwargs.get("dolfin_build_type", "Developer")
    builddir = "build/build"

    # Checkout
    f = factory.BuildFactory()
    f.addStep(steps.Git(repourl="https://bitbucket.org/fenics-project/dolfin.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Make build dir
    f.addStep(MakeDirectory(dir=builddir, hideStepIf=True))

    # Configure
    cmake_args = ["-DCMAKE_INSTALL_PREFIX:PATH=" + install_prefix,
                  #"-DDOLFIN_ENABLE_TESTING:BOOL=ON",
                  #"-DDOLFIN_ENABLE_BENCHMARKS:BOOL=ON",
                  "-DCMAKE_BUILD_TYPE:STRING=" + cmake_build_type]
    if extra_cmake_args is not None:
        assert(isinstance(extra_cmake_args, list))
        cmake_args.extend(extra_cmake_args)
    f.addStep(Configure(command=["cmake"] + cmake_args + [".."],
                        workdir=builddir,
                        logfiles={"CMakeCache.txt": "CMakeCache.txt",
                                  "CMakeError.log": "CMakeFiles/CMakeError.log"},
                        lazylogfiles=True))

    # Compile
    make_cmd = [make]
    if extra_make_args is not None:
        assert(isinstance(extra_make_args, list))
        make_cmd.extend(extra_make_args)
    f.addStep(Compile(command=make_cmd, workdir=builddir, timeout=50*60))

    # Install
    f.addStep(MakeInstall(command=make_cmd + ["install"], workdir=builddir))

    return f

def mk_dolfin_full_factory(branch="master", install_prefix=None,
                           coverage=False, remove_prefix=False, **kwargs):
    make = kwargs.get("make", os.environ.get("MAKE", "make"))
    extra_make_args = kwargs.get("dolfin_make_args", None)
    extra_cmake_args = kwargs.get("dolfin_cmake_args", None)
    cmake_build_type = kwargs.get("dolfin_build_type", "Developer")
    builddir = "build/build"
    sourcedir = ".."

    # Checkout
    f = factory.BuildFactory()
    f.addStep(steps.Git(repourl="https://bitbucket.org/fenics-project/dolfin.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Remove 'install_prefix' directory
    if remove_prefix:
        f.addStep(RemoveDirectory(dir=install_prefix,
                                  haltOnFailure=False,
                                  flunkOnFailure=False))

    # Make build directory
    f.addStep(MakeDirectory(dir=builddir, hideStepIf=True))
    
    # Configure - disable all optional packages
    cmake_args = [
        "-DCMAKE_INSTALL_PREFIX:PATH=" + install_prefix,
        "-DCMAKE_BUILD_TYPE:STRING=" + cmake_build_type,
        "-DDOLFIN_ENABLE_TESTING:BOOL=ON",
        "-DDOLFIN_ENABLE_BENCHMARKS:BOOL=ON",
        "-DDOLFIN_ENABLE_PETSC:BOOL=OFF",
        "-DDOLFIN_ENABLE_PETSC4PY:BOOL=OFF",
        "-DDOLFIN_ENABLE_SLEPC:BOOL=OFF",
        "-DDOLFIN_ENABLE_SLEPC4PY:BOOL=OFF",
        "-DDOLFIN_ENABLE_TRILINOS:BOOL=OFF",
        "-DDOLFIN_ENABLE_SCOTCH:BOOL=OFF",
        "-DDOLFIN_ENABLE_OPENMP:BOOL=OFF",
        "-DDOLFIN_ENABLE_MPI:BOOL=OFF",
        "-DDOLFIN_ENABLE_UMFPACK:BOOL=OFF",
        "-DDOLFIN_ENABLE_CHOLMOD:BOOL=OFF",
        "-DDOLFIN_ENABLE_PARMETIS:BOOL=OFF",
        "-DDOLFIN_ENABLE_ZLIB:BOOL=OFF",
        "-DDOLFIN_ENABLE_VTK:BOOL=OFF",
        "-DDOLFIN_ENABLE_PASTIX:BOOL=OFF",
        "-DDOLFIN_ENABLE_PYTHON:BOOL=OFF",
        "-DDOLFIN_ENABLE_HDF5:BOOL=OFF",
        "-DDOLFIN_ENABLE_QT:BOOL=OFF",
        ]
    if extra_cmake_args is not None:
        assert(isinstance(extra_cmake_args, list))
        cmake_args.extend(extra_cmake_args)
    f.addStep(ShellCommand(command=["cmake"] + cmake_args + [sourcedir],
                           description="configure (disable all)",
                           descriptionDone="configure (disable all)",
                           name="configure (disable all)",
                           workdir=builddir,
                           haltOnFailure=True, flunkOnFailure=True,
                           logfiles={"CMakeCache.txt": "CMakeCache.txt",
                                     "CMakeError.log": "CMakeFiles/CMakeError.log"},
                           lazylogfiles=True))

    # Compile
    make_cmd = [make]
    if extra_make_args is not None:
        assert(isinstance(extra_make_args, list))
        make_cmd.extend(extra_make_args)
    f.addStep(Compile(command=make_cmd, workdir=builddir, timeout=50*60))

    # Install
    f.addStep(ShellCommand(command=make_cmd + ["install"],
                           description="make install",
                           descriptionDone="make install",
                           name="make install",
                           workdir=builddir,
                           haltOnFailure=True, flunkOnFailure=True))

    # Compile tests
    f.addStep(Compile(command=make_cmd + ["test"],
                      description="make test",
                      descriptionDone="make test",
                      name="make test",
                      workdir=builddir))

    # Compile demos
    f.addStep(Compile(command=make_cmd + ["demo"],
                      description="make demo",
                      descriptionDone="make demo",
                      name="make demo",
                      workdir=builddir))

    # Compile benchmarks
    f.addStep(Compile(command=make_cmd + ["bench"],
                      description="make bench",
                      descriptionDone="make bench",
                      name="make bench",
                      workdir=builddir))

    # Configure - enable all optional packages
    cmake_args = [
        "-DCMAKE_INSTALL_PREFIX:PATH=" + install_prefix,
        "-DCMAKE_BUILD_TYPE:STRING=" + cmake_build_type,
        "-DDOLFIN_ENABLE_TESTING:BOOL=ON",
        "-DDOLFIN_ENABLE_BENCHMARKS:BOOL=ON",
        "-DDOLFIN_ENABLE_PETSC:BOOL=ON",
        "-DDOLFIN_ENABLE_PETSC4PY:BOOL=ON",
        "-DDOLFIN_ENABLE_SLEPC:BOOL=ON",
        "-DDOLFIN_ENABLE_SLEPC4PY:BOOL=ON",
        "-DDOLFIN_ENABLE_TRILINOS:BOOL=ON",
        "-DDOLFIN_ENABLE_SCOTCH:BOOL=ON",
        "-DDOLFIN_ENABLE_OPENMP:BOOL=ON",
        "-DDOLFIN_ENABLE_MPI:BOOL=ON",
        "-DDOLFIN_ENABLE_UMFPACK:BOOL=ON",
        "-DDOLFIN_ENABLE_CHOLMOD:BOOL=ON",
        "-DDOLFIN_ENABLE_PARMETIS:BOOL=ON",
        "-DDOLFIN_ENABLE_ZLIB:BOOL=ON",
        "-DDOLFIN_ENABLE_VTK:BOOL=ON",
        "-DDOLFIN_ENABLE_PASTIX:BOOL=ON",
        "-DDOLFIN_ENABLE_PYTHON:BOOL=ON",
        "-DDOLFIN_ENABLE_HDF5:BOOL=ON",
        "-DDOLFIN_ENABLE_QT:BOOL=ON",
        ]
    if extra_cmake_args is not None:
        assert(isinstance(extra_cmake_args, list))
        cmake_args.extend(extra_cmake_args)
    f.addStep(ShellCommand(command=["cmake"] + cmake_args + [sourcedir],
                           description="configure (enable all)",
                           descriptionDone="configure (enable all)",
                           name="configure (enable all)",
                           workdir=builddir,
                           haltOnFailure=True, flunkOnFailure=True,
                           logfiles={"CMakeCache.txt": "CMakeCache.txt",
                                     "CMakeError.log": "CMakeFiles/CMakeError.log"},
                           lazylogfiles=True))

    # Compile
    f.addStep(Compile(command=make_cmd,
                      description="make",
                      descriptionDone="make",
                      name="make",
                      workdir=builddir,
                      timeout=50*60))

    # Install
    f.addStep(ShellCommand(command=make_cmd + ["install"],
                           description="make install",
                           descriptionDone="make install",
                           name="make install",
                           workdir=builddir,
                           haltOnFailure=True, flunkOnFailure=True))

    # Compile tests
    f.addStep(Compile(command=make_cmd + ["tests"],
                      description="make tests",
                      descriptionDone="make tests",
                      name="make tests",
                      workdir=builddir))

    # Compile demos
    f.addStep(Compile(command=make_cmd + ["demo"],
                      description="make demo",
                      descriptionDone="make demo",
                      name="make demo",
                      workdir=builddir))

    # Compile benchmarks
    f.addStep(Compile(command=make_cmd + ["bench"],
                      description="make bench",
                      descriptionDone="make bench",
                      name="make bench",
                      workdir=builddir))

    # Run C++ unit tests
    f.addStep(ShellCommand(command=[make_cmd, "run_unittests_cpp"],
                           description="make run_unittests_cpp",
                           descriptionDone="make run_unittests_cpp",
                           name="make run_unittests_cpp",
                           workdir=builddir,
                           haltOnFailure=False, flunkOnFailure=True,
                           logfiles={"fail.log": "test/unit/fail.log"},
                           lazylogfiles=True,
                           timeout=20*60))

    # Run Python unit tests
    f.addStep(ShellCommand(command=[make_cmd, "run_unittests_py"],
                           description="make run_unittests_py",
                           descriptionDone="make run_unittests_py",
                           name="make run_unittests_py",
                           workdir=builddir,
                           haltOnFailure=False, flunkOnFailure=True,
                           logfiles={"fail.log": "test/unit/fail.log"},
                           lazylogfiles=True,
                           timeout=20*60))

    # Run Python unit tests with mpi
    f.addStep(ShellCommand(command=[make_cmd, "run_unittests_py_mpi"],
                           description="make run_unittests_py_mpi",
                           descriptionDone="make run_unittests_py_mpi",
                           name="make run_unittests_py_mpi",
                           workdir=builddir,
                           haltOnFailure=False, flunkOnFailure=True,
                           logfiles={"fail.log": "test/unit/fail.log",
                                     "output.0": "test/unit/python/output.0",
                                     "output.1": "test/unit/python/output.1",
                                     "output.2": "test/unit/python/output.2"},
                           lazylogfiles=True,
                           timeout=20*60))

    # Run regression tests
    f.addStep(ShellCommand(command=[make_cmd, "run_regressiontests"],
                           description="make run_regressiontests",
                           descriptionDone="make run_regressiontests",
                           name="make run_regressiontests",
                           workdir=builddir,
                           haltOnFailure=False, flunkOnFailure=True,
                           logfiles={"demo.log": "test/regression/demo.log"},
                           lazylogfiles=True,
                           timeout=20*60))

    # Run documentation tests
    f.addStep(ShellCommand(command=[make_cmd, "run_doctest"],
                           description="make run_doctest",
                           descriptionDone="make run_doctest",
                           name="make run_doctest",
                           workdir=builddir,
                           haltOnFailure=False, flunkOnFailure=True,
                           timeout=20*60))

    # Run documentation tests
    f.addStep(ShellCommand(command=[make_cmd, "run_styletest"],
                           description="make run_styletest",
                           descriptionDone="make run_styletest",
                           name="make run_styletest",
                           workdir=builddir,
                           haltOnFailure=False, flunkOnFailure=True,
                           timeout=20*60))

    if coverage:
        cmake_args.append("-DDOLFIN_ENABLE_CODE_COVERAGE")

    return f

def mk_dolfin_coverage_factory(branch="master", install_prefix=None, **kwargs):
    make = kwargs.get("make", os.environ.get("MAKE", "make"))
    extra_make_args = kwargs.get("dolfin_make_args", None)
    builddir = "build/build"
    lcovdir = builddir + "/lcov"

    # Checkout
    f = factory.BuildFactory()
    f.addStep(steps.Git(repourl="https://bitbucket.org/fenics-project/dolfin.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Make directories
    f.addStep(MakeDirectory(dir=builddir, hideStepIf=True))
    f.addStep(MakeDirectory(dir=lcovdir, hideStepIf=True))

    # Configure - enable coverage
    f.addStep(Configure(command=["cmake",
                                 "-DCMAKE_INSTALL_PREFIX:PATH=" + install_prefix,
                                 "-DDOLFIN_ENABLE_TESTING:BOOL=ON",
                                 "-DDOLFIN_ENABLE_CODE_COVERAGE:BOOL=ON",
                                 ".."],
                        workdir=builddir,
                        logfiles={"CMakeCache.txt": "CMakeCache.txt",
                                  "CMakeError.log": "CMakeFiles/CMakeError.log"},
                        lazylogfiles=True))

    # Compile
    make_cmd = [make]
    if extra_make_args is not None:
        assert(isinstance(extra_make_args, list))
        make_cmd.extend(extra_make_args)
    f.addStep(Compile(command=make_cmd, workdir=builddir))

    # Install
    f.addStep(MakeInstall(command=make_cmd + ["install"], workdir=builddir))

    # Compile tests
    f.addStep(ShellCommand(command=make_cmd + ["test"],
                           workdir=builddir,
                           description="compiling tests",
                           descriptionDone="compile tests",
                           name="compiling tests",
                           haltOnFailure=True, flunkOnFailure=True))

    # FIXME: DISABLE_PARALLEL_TESTING in env?
    f.addStep(Test(command=[make, "run_quicktest"], workdir=builddir))

    # FIXME: Make empty .gcda files for each existing .gcno file?

    # Generate
    f.addStep(
        ShellCommand(command=textwrap.dedent("""
        echo "========== Generating html coverage reports in lcov =========="
        lcov --directory . --capture --output-file lcov/all.info || true;
        lcov --remove lcov/all.info \
            "/home/buildbot/buildbots/main/fenics-build/*" \
            "/home/buildbot/buildbots/main/local/*" \
            "/home/buildbot/buildbots/main/fenics/*" \
            "/home/buildbot/buildbots/main/fenics-src/dolfin/test/*" \
            "/usr/*" \
            "/home/buildbot/buildbots/main/fenics-src/dolfin/demo/*" \
            --output lcov/lcov.info || true;
        #    "/home/buildbot/buildbots/main/fenics-src/dolfin/bench/*" \
        #    "/home/buildbot/buildbots/main/fenics-src/dolfin/demo/*" \
        #    "/home/buildbot/buildbots/main/fenics-src/dolfin/test/*" \
        #    "/home/buildbot/buildbots/main/fenics-build/dolfin/*" \
        #    "/opt/*" \
        #genhtml -o lcov lcov/all.info || LCOVFAILED=1
        genhtml -o lcov lcov/lcov.info || LCOVFAILED=1;
        if [ $LCOVFAILED ]; then
            cat > lcov/index.html <<EOF
<html><head><title>No coverage report</title></head>
<body>No coverage report for this module</body>
</html>
EOF
        fi
    """),
                     description='coverage',
                     descriptionDone='coverage',
                     name='coverage report',
                     workdir=builddir,
                     haltOnFailure=True, flunkOnFailure=True))

    ## Upload coverage report
    f.addStep(DirectoryUpload(slavesrc="lcov",
                              masterdest=public_html + "/coverage/dolfin",
                              url="http://fenicsproject.org:8010/coverage/dolfin",
                              workdir=builddir))
    return f

def mk_dolfin_docs_factory(branch="master", install_prefix=None, **kwargs):
    make = kwargs.get("make", os.environ.get("MAKE", "make"))
    extra_make_args = kwargs.get("dolfin_make_args", None)
    builddir = "build/build"

    # Checkout
    f = factory.BuildFactory()
    f.addStep(steps.Git(repourl="https://bitbucket.org/fenics-project/dolfin.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Make build dir
    f.addStep(MakeDirectory(dir=builddir, hideStepIf=True))

    # Get common fenics template from fenics-web
    f.addStep(
        ShellCommand(command=textwrap.dedent("""
FENICS_WEB_DIR=%s/fenics-web;
if [ ! -d $FENICS_WEB_DIR ]; then
  git clone https://bitbucket.org/fenics-project/fenics-web.git $FENICS_WEB_DIR;
else
  cd $FENICS_WEB_DIR && git pull && cd -;
fi
rsync -av $FENICS_WEB_DIR/source/_themes/ doc-old/sphinx-common/source/_themes/;
sed -i "s/^html_theme = .*/html_theme = 'fenics-bootstrap'/" doc-old/sphinx-common/source/conf.py
""" % install_prefix),
                     description='setting fenics theme',
                     descriptionDone='set fenics theme',
                     name='fenics theme',
                     haltOnFailure=False, flunkOnFailure=False))

    # Configure - enable docs
    f.addStep(Configure(command=["cmake",
                                 "-DCMAKE_INSTALL_PREFIX:PATH=" + install_prefix,
                                 "-DDOLFIN_ENABLE_DOCS:BOOL=ON",
                                 ".."],
                        workdir=builddir,
                        logfiles={"CMakeCache.txt": "CMakeCache.txt",
                                  "CMakeError.log": "CMakeFiles/CMakeError.log"},
                        lazylogfiles=True))

    # Compile
    make_cmd = [make]
    if extra_make_args is not None:
        assert(isinstance(extra_make_args, list))
        make_cmd.extend(extra_make_args)
    f.addStep(Compile(command=make_cmd, workdir=builddir, timeout=30*60))

    # Install
    if install_prefix is not None:
        f.addStep(MakeInstall(command=make_cmd + ["install"], workdir=builddir))

    # Build documentation
    f.addStep(ShellCommand(command=["make", "doc"],
                           description="make doc",
                           descriptionDone="make doc",
                           name="make doc",
                           workdir=builddir,
                           flunkOnFailure=True, haltOnFailure=True))

    # Install documentation
    f.addStep(ShellCommand(command=["make", "doc_install"],
                           description="make doc_install",
                           descriptionDone="make doc_install",
                           name="make doc_install",
                           workdir=builddir,
                           flunkOnFailure=True, haltOnFailure=True))

    # Upload documentation
    masterdest = public_html + "/docs/dolfin-"
    f.addStep(DirectoryUpload(slavesrc="doc-old/sphinx-cpp/build/html",
                              masterdest=masterdest + branch + "/cpp",
                              url="http://fenicsproject.org:8010/docs/dolfin-%s/cpp" % branch,
                              workdir=builddir))
    f.addStep(DirectoryUpload(slavesrc="doc-old/sphinx-python/build/html",
                              masterdest=masterdest + branch + "/python",
                              url="https://fenicsproject.org:8010/docs/dolfin-%s/python" % branch,
                              workdir=builddir))

    # Set permission on master
    f.addStep(MasterShellCommand(command="chmod -R a+rX %s/docs" % public_html))

    return f

def mk_ufl_docs_factory(branch="master", install_prefix=None, **kwargs):
    python = kwargs.get("python", os.environ.get("PYTHON", "python"))
    make = kwargs.get("make", os.environ.get("MAKE", "make"))

    # Checkout
    f = factory.BuildFactory()
    f.addStep(steps.Git(repourl="https://bitbucket.org/fenics-project/ufl.git",
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Only install if a prefix is given
    if install_prefix is not None:
        f.addStep(PythonInstall(command=[python, "setup.py", "install",
                                         "--prefix=" + install_prefix]))

    # Get common fenics template from fenics-web
    f.addStep(
        ShellCommand(command=textwrap.dedent("""
FENICS_WEB_DIR=%s/fenics-web;
if [ ! -d $FENICS_WEB_DIR ]; then
  git clone https://bitbucket.org/fenics-project/fenics-web.git $FENICS_WEB_DIR;
else
  cd $FENICS_WEB_DIR && git pull && cd -;
fi
rsync -av $FENICS_WEB_DIR/source/_themes/ doc/sphinx/source/_themes/;
sed -i "s/^#html_theme = .*/html_theme = 'fenics-bootstrap'/" doc/sphinx/source/conf.py
sed -i "s/^#html_theme_path = .*/html_theme_path = ['_themes']/" doc/sphinx/source/conf.py
""" % install_prefix),
                     description='setting fenics theme',
                     descriptionDone='set fenics theme',
                     name='fenics theme',
                     haltOnFailure=False, flunkOnFailure=False))

    # Build documentation
    f.addStep(ShellCommand(command=["make", "html"],
                           description="generating doc",
                           descriptionDone="generate doc",
                           name="generate doc",
                           workdir="build/doc/sphinx",
                           haltOnFailure=True, flunkOnFailure=True))

    # Upload documentation
    f.addStep(DirectoryUpload(slavesrc="doc/sphinx/build/html",
                              masterdest=public_html + "/docs/ufl-" + branch,
                              url="http://fenicsproject.org:8010/docs/ufl-" + branch))

    # Set permission on master
    f.addStep(MasterShellCommand(command="chmod -R a+rX %s/docs" % public_html))

    return f

def mk_snapshot_factory(project, branch="master", **kwargs):
    f = factory.BuildFactory()

    # Checkout
    f.addStep(steps.Git(repourl="https://bitbucket.org/fenics-project/%s.git" % project,
                        branch=branch, mode='full', retry=GIT_RETRY))

    # Generate snapshot
    tarball = project + "-snapshot.tar.gz"
    f.addStep(ShellCommand(command=textwrap.dedent("""
git archive --format=tar.gz --prefix=%s-snapshot/ %s > %s
""" % (project, branch, tarball)),
                           description="generating snapshot",
                           descriptionDone="generate snapshot",
                           name="generate snapshot",
                           haltOnFailure=True, flunkOnFailure=True))

    # Upload snapshot
    f.addStep(DirectoryUpload(slavesrc=tarball,
                              masterdest=public_html + "/snapshots/" + tarball,
                              url="http://fenicsproject.org:8010/snapshots/" + tarball))

    # Set permission on master
    f.addStep(MasterShellCommand(command="chmod -R a+rX %s/snapshots" % public_html))

    return f

#### docs, coverage, etc.

for sl in get_slaves(documentation=True).values():
    for branch in sl.branches:
        prefix = sl.install_prefix + "/" + branch
        builder_env = sl.env.copy()
        builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)

        # Install into "docs" subdir
        prefix = prefix + "/docs"
        builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)

        project = "dolfin"
        builder_name = '%s-%s-docs-%s' % (project, branch, sl.slavename)
        f = mk_dolfin_docs_factory(branch=branch, install_prefix=prefix)
        builders.append({
            'name': builder_name,
            'slavenames': [sl.slavename],
            'factory': f,
            'category': "docs",
            'env': builder_env,
            })

        project = "ufl"
        builder_name = '%s-%s-docs-%s' % (project, branch, sl.slavename)
        f = mk_ufl_docs_factory(branch=branch, install_prefix=prefix)
        builders.append({
            'name': builder_name,
            'slavenames': [sl.slavename],
            'factory': f,
            'category': "docs",
            'env': builder_env,
            })

for sl in get_slaves(coverage=True).values():
    # FIXME: We only generate coverage reports for master for now
    for branch in ["master"]:
        prefix = sl.install_prefix + "/" + branch
        builder_env = sl.env.copy()
        builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)

        # Install into "coverage" subdir
        prefix = prefix + "/coverage"
        builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)

        project = "dolfin"
        builder_name = '%s-%s-coverage-%s' % (project, branch, sl.slavename)
        f = mk_dolfin_coverage_factory(branch=branch, install_prefix=prefix)
        builders.append({
            'name': builder_name,
            'slavenames': [sl.slavename],
            'factory': f,
            'category': "docs",
            'env': builder_env,
            })

for sl in get_slaves(snapshot=True).values():
    # NOTE: We only generate snapshots for master
    for branch in ["master"]:
        prefix = sl.install_prefix + "/" + branch
        builder_env = sl.env.copy()
        builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)

        for project in ["dijitso", "dolfin", "ffc", "fiat", "mshr", "instant", "ufl"]:
            f = mk_snapshot_factory(project, branch=branch)
            builders.append({
                'name': '%s-%s-snapshot-%s' % (project, branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "dist",
                })

#### single-slave builders

for sl in get_slaves(run_single=True).values():
    for branch in sl.branches:
        prefix = sl.install_prefix + "/" + branch
        builder_env = sl.env.copy()
        builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)

    for branch in sl.branches:
        prefix = sl.install_prefix + "/" + branch
        builder_env = sl.env.copy()
        builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)
        builder_env['INSTANT_CACHE_DIR'] = prefix + "/instant-cache"
        builder_env['INSTANT_ERROR_DIR'] = prefix + "/instant-error"

        for project in ["dijitso", "ffc", "fiat", "instant", "ufl"]:
            f = mk_python_factory(project, branch=branch,
                                  install_prefix=prefix, env=builder_env)
            builders.append({
                'name': '%s-%s-%s' % (project, branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, branch),
                'env': builder_env,
                })

        if sl.use_simple:
            project = "dolfin"
            f = mk_dolfin_quick_factory(branch=branch, install_prefix=prefix,
                                        **sl.kwargs)
            builders.append({
                'name': '%s-%s-quick-%s' % (project, branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, branch),
                'env': builder_env,
                })

        if branch == "master":
            project = "mshr"
            f = mk_mshr_factory(branch=branch, install_prefix=prefix)
            builders.append({
                'name': '%s-%s-%s' % (project, branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, branch),
                'env': builder_env,
                })

        if sl.use_full:
            project = "dolfin"
            # Install into "dolfin-full" subdir
            prefix = prefix + "/dolfin-full"
            builder_env = update_env(prefix, builder_env, os=sl.os, pyver=sl.pyver)

            f = mk_dolfin_full_factory(branch=branch, install_prefix=prefix,
                                       remove_prefix=True, **sl.kwargs)
            builders.append({
                'name': '%s-%s-full-%s' % (project, branch, sl.slavename),
                'slavenames': [sl.slavename],
                'factory': f,
                'category': "%s.%s" % (project, branch),
                'env': builder_env,
                })


#### operating systems

#for opsys in set(sl.os for sl in slaves if sl.os is not None):
#    if 'win' in opsys:
#        f = mksimplefactory(test_master=sl.test_master)
#    else:
#        f = mktestfactory()
#    builders.append({
#        'name' : 'os-%s' % opsys,
#        'slavenames' : names(get_slaves(os=opsys)),
#        'factory' : f,
#        'category' : 'os' })
