from buildbot.changes.gitpoller import GitPoller

changes = []

repourl = 'https://bitbucket.org/fenics-project/%s.git'
for project in ['dijitso', 'dolfin', 'ffc', 'fiat', 'instant', 'mshr', 'ufl']:
    changes.append(GitPoller(repourl=repourl % project,
                             pollinterval=5*60,
                             branches=['master'],
                             project=project,
                             category=project + '.master'
                             ))
