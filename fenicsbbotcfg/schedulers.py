schedulers = []

from buildbot.schedulers.basic import Scheduler, SingleBranchScheduler
from buildbot.schedulers.forcesched import ForceScheduler
from buildbot.schedulers.timed import Nightly
from buildbot.changes import filter

from buildbot.schedulers.timed import Periodic
from buildbot.schedulers.trysched import Try_Userpass
from fenicsbbotcfg.slaves import slaves
from fenicsbbotcfg import builders

import os

def get_pass(name):
    # get the password based on the name
    path = os.path.join(os.path.dirname(__file__), "%s.pass" % name)
    pw = open(path).read().strip()
    return pw

schedulers.append(ForceScheduler(name="force",
                                 properties=[],
                                 builderNames=[b['name'] for b in builders.builders]))

# Nightly schedulers for dijitso
schedulers.append(Nightly(name="nightly-dijitso-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('dijitso-master')],
                          hour=1, minute=30))
schedulers.append(Nightly(name="nightly-dijitso-next",
                          branch="next",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('dijitso-next')],
                          hour=1, minute=40))

# Nightly schedulers for FIAT
schedulers.append(Nightly(name="nightly-fiat-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('fiat-master')],
                          hour=2, minute=0))
schedulers.append(Nightly(name="nightly-fiat-next",
                          branch="next",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('fiat-next')],
                          hour=2, minute=0))

# Nightly schedulers for Instant
schedulers.append(Nightly(name="nightly-instant-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('instant-master')],
                          hour=2, minute=0))
schedulers.append(Nightly(name="nightly-instant-next",
                          branch="next",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('instant-next')],
                          hour=2, minute=10))

# Nightly schedulers for UFL
schedulers.append(Nightly(name="nightly-ufl-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('ufl-master')],
                          hour=2, minute=0))
schedulers.append(Nightly(name="nightly-ufl-next",
                          branch="next",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('ufl-next')],
                          hour=2, minute=0))

# Nightly schedulers for FFC
schedulers.append(Nightly(name="nightly-ffc-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('ffc-master')],
                          hour=2, minute=30))
schedulers.append(Nightly(name="nightly-ffc-next",
                          branch="next",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('ffc-next')],
                          hour=2, minute=30))

# Nightly schedulers for DOLFIN
schedulers.append(Nightly(name="nightly-dolfin-master-quick",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('dolfin-master-quick')],
                          hour=3, minute=0))
schedulers.append(Nightly(name="nightly-dolfin-next-quick",
                          branch="next",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('dolfin-next-quick')],
                          hour=3, minute=0))
schedulers.append(Nightly(name="nightly-dolfin-master-full",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('dolfin-master-full')],
                          hour=4, minute=0))
schedulers.append(Nightly(name="nightly-dolfin-next-full",
                          branch="next",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('dolfin-next-full')],
                          hour=4, minute=0))
schedulers.append(Nightly(name="nightly-dolfin-master-docs-misc",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('dolfin-master-docs')],
                          hour=4, minute=0))

# Nightly schedulers for mshr
schedulers.append(Nightly(name="nightly-mshr-master",
                          branch="master",
                          builderNames=[b['name'] for b in builders.builders \
                                        if b['name'].startswith('mshr-master')],
                          hour=5, minute=30))

# Allow core members to do try builds
core_members = ["logg", "kehlet", "wells", "hake", "ring",
                "mardal", "oelgaard", "rognes", "alnes", "richardson",
                # the following are not core members, but have try access
                "markall", "farrell",
                ]

schedulers.append(
    Try_Userpass(name="try_core",
                 builderNames=[b['name'] for b in builders.builders],
                 port=8031,
                 userpass=[(n, get_pass(n)) for n in core_members]))

for project in ["dijitso", "dolfin", "ffc", "fiat", "instant", "mshr", "ufl"]:
    for branch in ["master", "next"]:
        if project == "mshr" and branch != "master":
            continue  # mshr has only a master branch
        schedulers.append(
            SingleBranchScheduler(
                name="%s-%s" % (project, branch),
                builderNames=[b['name'] for b in builders.builders \
                              if b['name'].startswith(project + "-" + branch)],
                change_filter=filter.ChangeFilter(
                    project=project,
                    branch=branch,
                    category="%s.%s" % (project, branch),
                    )))
