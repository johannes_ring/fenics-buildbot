import sys, os
from buildbot.buildslave import BuildSlave

class MySlaveBase(object):
    # Where should the software be installed?
    install_prefix = "/home/buildbot/fenicsbbot"

    # Define evinronment variablen for slave
    env = {}
    #env["PATH"] = ":".join([prefix + "/bin", env.get("PATH", ""), "${PATH}"])
    #env["LD_LIBRARY_PATH"] = ":".join([prefix + "/lib",
    #                                   env.get("LD_LIBRARY_PATH", ""),
    #                                   "${LD_LIBRARY_PATH}"])
    #env["PKG_CONFIG_PATH"] = ":".join([prefix + "/lib/pkgconfig",
    #                                   env.get("PKG_CONFIG_PATH", ""),
    #                                   "${PKG_CONFIG_PATH}"])
    #env["PYTHONPATH"] = ":".join([prefix + "/lib/python" + pyver + "/site-packages",
    #                              env.get("PYTHONPATH", ""),
    #                              "${PYTHONPATH}"])

    # Keyword arguments to be sent to builders
    kwargs = {}

    # true if this box is fenicsproject.org, and can build docs, etc.
    fenicsproject_org = False

    # Python version on this slave
    pyver = "2.7"

    # true if this box can generate documentation.
    documentation = False

    # true if this box can run benchmarks.
    benchmark = False

    # true if this box can generate coverage reports
    coverage = False

    # true if this box should generate and upload snapshots
    snapshot = False

    # true if this box should use a 'simple' factory
    use_simple = True

    # true if this box should use a 'full' factory
    use_full = True

    # true if this slave should have a single-slave builder of its own
    run_single = True

    # operating system
    os = None

    # branches to build
    branches = ["master", "next"]

    def extract_attrs(self, name, **kwargs):
        self.slavename = name
        remaining = {}
        for k in kwargs:
            if hasattr(self, k):
                setattr(self, k, kwargs[k])
            else:
                remaining[k] = kwargs[k]
        return remaining

    def get_pass(self, name):
        # get the password based on the name
        path = os.path.join(os.path.dirname(__file__), "%s.pass" % name)
        pw = open(path).read().strip()
        return pw

    def get_ec2_creds(self, name):
        path = os.path.join(os.path.dirname(__file__), "%s.ec2" % name)
        return open(path).read().strip().split(" ")

class MySlave(MySlaveBase, BuildSlave):
    def __init__(self, name, **kwargs):
        password = self.get_pass(name)
        kwargs = self.extract_attrs(name, **kwargs)
        BuildSlave.__init__(self, name, password, **kwargs)

slaves = [
    #MySlave('sid-amd64',
    #        fenicsproject_org=True,
    #        max_builds=2,
    #        install_prefix='/home/johannr/fenicsbbot',
    #        coverage=True,
    #        snapshot=True,
    #        env={'PETSC_DIR': '/usr/lib/petscdir/3.2',
    #             'SLEPC_DIR': '/usr/lib/slepcdir/3.2',
    #             #'PATH': ['/home/johannr/local/bin', '${PATH}'],
    #             },
    #        ),

    MySlave('sid-amd64',  # ssh buildbot@192.168.101.175
            max_builds=3,
            kwargs={'dolfin_cmake_args': [
                        '-DHDF5_C_COMPILER_EXECUTABLE:FILEPATH=/usr/bin/h5pcc',
                        ],
                    },
            ),

    MySlave('jessie-amd64',  # ssh buildbot@192.168.100.213
            max_builds=3,
            ),

    MySlave('trusty-amd64',  # ssh buildbot@192.168.203.96
            max_builds=2,
            ),

    MySlave('trusty-amd64-py3',  # ssh buildbot@192.168.203.80
            max_builds=2,
            pyver="3.4",
            kwargs={'dolfin_cmake_args': [
                        '-DPYTHON_INCLUDE_DIRS:PATH=/usr/include/python3.4m',
                        '-DPYTHON_EXECUTABLE:PATH=/usr/bin/python3',
                        '-DPYTHON_LIBRARY:FILEPATH=/usr/lib/x86_64-linux-gnu/libpython3.4m.so',
                        ],
                    },
            ),

    #MySlave('precise-amd64',  # ssh buildbot@192.168.101.35
    #        max_builds=3,
    #        ),
    #
    #MySlave('precise-i386',  # ssh buildbot@192.168.101.80
    #        max_builds=3,
    #        kwargs={'dolfin_cmake_args': ['-DDOLFIN_ENABLE_SCOTCH:BOOL=OFF']},
    #        ),

    #MySlave('osx-10.7',  # ssh buildbot@192.168.201.195
    #        max_builds=3,
    #        install_prefix='/Users/buildbot/fenicsbbot',
    #        os='osx',
    #        kwargs={'dolfin_build_type': 'Release',
    #                'dolfin_cmake_args': [
    #                    '-DPYTHON_INCLUDE_DIR:PATH=/opt/local/Library/Frameworks/Python.framework/Versions/2.7/include/python2.7',
    #                    '-DPYTHON_LIBRARY:FILEPATH=/opt/local/lib/libpython2.7.dylib',
    #                    '-DCMAKE_CXX_FLAGS_RELEASE=\'-O3 -DNDEBUG -DDEBUG\'',
    #                    ],
    #                },
    #        ),

    #MySlave('osx-10.8',  # ssh buildbot@192.168.201.40
    #        max_builds=2,
    #        install_prefix='/Users/buildbot/fenicsbbot',
    #        os='osx',
    #        kwargs={'dolfin_build_type': 'Release',
    #                'dolfin_cmake_args': [
    #                    '-DPYTHON_INCLUDE_DIR:PATH=/opt/local/Library/Frameworks/Python.framework/Versions/2.7/include/python2.7',
    #                    '-DPYTHON_LIBRARY:FILEPATH=/opt/local/lib/libpython2.7.dylib',
    #                    '-DCMAKE_CXX_FLAGS_RELEASE=\'-O3 -DNDEBUG -DDEBUG\'',
    #                    ],
    #               },
    #        ),

    #MySlave('osx-10.9',  # ssh buildbot@192.168.202.176
    #        max_builds=2,
    #        install_prefix='/Users/buildbot/fenicsbbot',
    #        os='osx',
    #        kwargs={'dolfin_build_type': 'Release',
    #               },
    #        ),

    MySlave('mpich',  # ssh buildbot@192.168.101.208
            max_builds=3,
            ),

    MySlave('misc',  # ssh buildbot@192.168.101.37
            max_builds=3,
            use_full=False,
            snapshot=True,
            documentation=True,
            ),

#    # Local
#    MySlave('fenicsproject.org',
#        fenicsproject_org=True,
#        run_config=False,
#        run_single=False,
#        max_builds=1,
#        ),
]

# these are slaves that haven't been up and from whose owners I have
# not heard in a while
retired_slaves = [
]

def get_slaves(db=None, *args, **kwargs):
    rv = {}
    for arg in args:
        rv.update(arg)
    for sl in slaves:
        if db and db not in sl.databases:
            continue
        for k in kwargs:
            if getattr(sl, k) != kwargs[k]:
                break
        else:
            rv[sl.slavename] = sl
    return rv

def names(slavedict):
    return slavedict.keys()
